using UnityEngine.InputSystem.EnhancedTouch;
using UnityEngine;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;
using TouchPhase = UnityEngine.InputSystem.TouchPhase;

[DefaultExecutionOrder(-10)]
public class TouchInputManager : Singleton<TouchInputManager>
{

    public delegate void TouchBeginDelegate(Touch touch);
    public event TouchBeginDelegate TouchBeginEvent;

    public delegate void TouchProcessDelegate(Touch touch);
    public event TouchProcessDelegate TouchProcessEvent;

    public delegate void TouchEndedDelegate(Touch touch);
    public event TouchEndedDelegate TouchEndedEvent;
    
    void Awake()
    {
        EnhancedTouchSupport.Enable();
    }
    
    void Update()
    {
        if (Touch.activeTouches.Count > 0)
        {
            switch (Touch.activeTouches[0].phase)
            {
                case TouchPhase.Began:
                    if (!PointerOverUIElement.IsPointerOverUI()) TouchBeginEvent?.Invoke(Touch.activeTouches[0]);
                    break;
                case TouchPhase.Moved:
                case TouchPhase.Stationary:
                    TouchProcessEvent?.Invoke(Touch.activeTouches[0]);
                    break;
                case TouchPhase.Ended:
                case TouchPhase.Canceled:
                    TouchEndedEvent?.Invoke(Touch.activeTouches[0]);
                    break;
            }
        }
    }
}
