using UnityEngine.UIElements;

public static class PointerOverUIElement
{
    private static UQueryBuilder<TextElement> _toolButtons;

    private static bool _pointerOverUI;

    public static bool IsPointerOverUI()
    {
        return _pointerOverUI;
    }

    public static void RegisterAllUIElementsInRoot(VisualElement rootVisualElement)
    {
        _pointerOverUI = false;
        if (_toolButtons.Equals(null))
        {
            _toolButtons.ForEach(button => button.UnregisterCallback<PointerMoveEvent>(PointerOverUI));
            _toolButtons.ForEach(button => button.UnregisterCallback<PointerLeaveEvent>(PointerLeaveUI));
        }

        _toolButtons = rootVisualElement.Query<TextElement>();

        _toolButtons.ForEach(button => button.RegisterCallback<PointerMoveEvent>(PointerOverUI));
        _toolButtons.ForEach(button => button.RegisterCallback<PointerLeaveEvent>(PointerLeaveUI));
    }

    private static void PointerLeaveUI(PointerLeaveEvent evt)
    {
        _pointerOverUI = false;
    }

    private static void PointerOverUI(PointerMoveEvent evt)
    {
        _pointerOverUI = true;
    }
}
