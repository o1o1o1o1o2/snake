using UnityEngine;
using UnityEngine.InputSystem;

public class SnakeInputController : Singleton<SnakeInputController>
{
    private SnakeMovement _snakeMovement;
    private GameSettings _gameSettings;

    private Vector2 _startPos;
    private Vector2 _endPos;
    private bool _touchBegan;

    public void Constructor(SnakeMovement snakeMovement, GameSettings gameSettings)
    {
        _snakeMovement = snakeMovement;
        _gameSettings = gameSettings;
    }

    private void Awake()
    {
        TouchInputManager.Instance.TouchBeginEvent += OnTouchBegin;
        TouchInputManager.Instance.TouchEndedEvent += OnTouchEnded;
        
    }
    
    public void ButtonUpPressed()
    { 
        _snakeMovement.SetNewMoveDirection(Vector2.up);
    }
    public void ButtonLeftPressed()
    {
       _snakeMovement.SetNewMoveDirection(Vector2.left);
    }
    public void ButtonRightPressed()
    {
         _snakeMovement.SetNewMoveDirection(Vector2.right);
    }
    public void ButtonDownPressed()
    {
       _snakeMovement.SetNewMoveDirection(Vector2.down);
    }

    public void OnMovement(InputAction.CallbackContext ctx)
    {
        if (ctx.performed) _snakeMovement.SetNewMoveDirection(ctx.ReadValue<Vector2>());
    }

    private void OnTouchBegin(UnityEngine.InputSystem.EnhancedTouch.Touch touch)
    {
        _touchBegan = true;
        _startPos = touch.screenPosition;
    }

    private void OnTouchEnded(UnityEngine.InputSystem.EnhancedTouch.Touch touch)
    {
        if (!_touchBegan) return;

        _endPos = touch.screenPosition;

        Vector2 delta = _endPos - _startPos;
        if (Mathf.Abs(delta.x) > Mathf.Abs(delta.y))
        {
            if (delta.x > 0) _snakeMovement.SetNewMoveDirection(new Vector2(1, 0));
            else if (delta.x < 0) _snakeMovement.SetNewMoveDirection(new Vector2(-1, 0));
        }
        else
        {
            if (delta.y > 0) _snakeMovement.SetNewMoveDirection(new Vector2(0, 1));
            else if (delta.y < 0) _snakeMovement.SetNewMoveDirection(new Vector2(0, -1));
        }

        _touchBegan = false;
    }
}
