using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class FoodSpawner
{
    public Vector2Int foodPos { get; private set; }
    
    private readonly GameSettings _gameSettings;
    private readonly Transform _foodTr;
    private readonly Renderer _renderer;
    private readonly Vector3 _offset = new Vector3(0.5f,0.5f, 0f);
    private Vector2Int _size;
    private Color _foodColor;

    public Color foodColor
    {
        get => _foodColor;
        private set => _foodColor = value;
    }
    
    private List<Vector2Int> _unlockedGrids;

    public void RemoveUnlockedGrids(Vector2Int position)
    {
        _unlockedGrids.Remove(position);
    }
    
    public void AddUnlockedGrids(Vector2Int position)
    {
        _unlockedGrids.Add(position);
    }
    
    public int GetUnlockedGridsLength()
    {
        return _unlockedGrids.Count;
    }

    public FoodSpawner(GameObject prefab, GameSettings gameSettings)
    {
        _gameSettings = gameSettings;

        _size = _gameSettings.gridSize;

        var go = Object.Instantiate(prefab);
        
        _renderer = go.GetComponentInChildren<Renderer>();
        
        if (_gameSettings.singleColor) _renderer. sharedMaterial.color = new Color(0.5f,0.1f,0.1f,1f);

        _foodTr = go.transform;
    }
    
    public void FillUnlockedGridsList()
    {
        _unlockedGrids?.Clear();
        
        _unlockedGrids = new List<Vector2Int>();
        
        for (int i = 0; i < _size.x; i++)
        {
            for (int j = 0; j < _size.y; j++)
            {
                _unlockedGrids.Add(new Vector2Int(i,j));
            }
        }
    }

    public void SpawnFood()
    {
        int index = Random.Range(0,_unlockedGrids.Count - 1);
         foodPos = _unlockedGrids[index];
         _foodTr.position = new Vector3(foodPos.x,foodPos.y) + _offset;

         if (!_gameSettings.singleColor)
         {
             _foodColor = Random.ColorHSV(0.0f, 1.0f, 0.7f, 1.0f, 0.4f, 0.9f);
             _renderer.sharedMaterial.color = _foodColor;
         }
    }
}
