using System;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "SnakeGameSettings", menuName = "Snake/SnakeGameSettings", order = 1)]
public class GameSettings : ScriptableObject
{
    public bool singleColor;
    public float startMoveSpeed;
    public bool godMode;

    [SerializeField, SerializeProperty("gridSize")]
    private Vector2Int _gridSize;
    public Vector2Int gridSize
    {
        get => _gridSize;
        set {
            if (value.x >=0 && value.y>=0)
            {
                _gridSize = value / 2 * 2 + Vector2Int.one;
                onGridSizeChanged.Invoke();
            }
        }
    }

    [SerializeField]
    private UnityEvent onGridSizeChanged;
}
