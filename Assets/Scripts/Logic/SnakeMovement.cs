using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SnakeMovement : MonoBehaviour
{
    [SerializeField]
    private UnityEvent snakeGrow;
    [SerializeField]
    private UnityEvent gameOver;
    private GameSettings _gameSettings;
    private FoodSpawner _foodSpawner;
    
    private GameObject _snakePartPrefab;
    private MeshFilter _meshFilter;
    private Color[] _colors;
    
    [SerializeField]
    private IntReference snakeSize;
    private Vector2Int _snakeHeadGridPos;

    private bool _directionChanged;
    private Vector2Int _moveDirection;
    private List<Vector2Int> _snakePathGridPositions;
    private List<Transform> _snakePartsTransforms;
    
    private readonly Vector2 _offset = new Vector2(0.5f,0.5f);

    private float _moveSpeed;
    private float _timer;

    private bool _mooving;
    private Transform snakeHeadTransform; 
    
    public void StartMoving()
    {
        snakeSize.Variable.ReInit();
        _moveSpeed = _gameSettings.startMoveSpeed;
        
        for (int i = 1; i < _snakePartsTransforms.Count; i++)
        {
            DestroyImmediate(_snakePartsTransforms[i].gameObject); //todo pool
        }

        _foodSpawner.FillUnlockedGridsList();
        _foodSpawner.SpawnFood();
        
        _moveDirection = new Vector2Int(0,1);
        _snakeHeadGridPos = new Vector2Int(_gameSettings.gridSize.x / 2, _gameSettings.gridSize.y / 2);
        
        _snakePartsTransforms.Clear();
        _snakePathGridPositions.Clear();
        
        _snakePartsTransforms.Add(snakeHeadTransform);
        _snakePathGridPositions.Add(_snakeHeadGridPos);
        _foodSpawner.RemoveUnlockedGrids(_snakeHeadGridPos);
        
        _mooving = true;
    }

    public void Constructor(GameSettings gameSettings, FoodSpawner foodSpawner, GameObject prefab)
    {
        _gameSettings = gameSettings;
        _foodSpawner = foodSpawner;
        _snakePartPrefab = prefab;
        _moveSpeed = gameSettings.startMoveSpeed;
    }
    private void Awake()
    {
        _snakePartsTransforms = new List<Transform>();
        _snakePathGridPositions = new List<Vector2Int>();
        
        var go = Instantiate(_snakePartPrefab);
        snakeHeadTransform = go.transform;
        if (!_gameSettings.singleColor)
        {
            _meshFilter = go.GetComponent<MeshFilter>();
            _colors = new Color[_meshFilter.mesh.vertexCount];
            SetMeshVertexColor(Color.blue);
            _meshFilter.mesh.colors = _colors;
        }
        else
        {
            go.GetComponent<Renderer>().sharedMaterial = new Material(Shader.Find("Diffuse")) {color = new Color(0.15f,0.3f,0.15f,1)};
            _snakePartPrefab = go;
        }
    }

    private void Update()
    {
        if (_mooving) Move();
    }

    private void Move()
    {
        _timer += Time.deltaTime;
        if (_timer >= _moveSpeed || _directionChanged)
        {
            _snakeHeadGridPos += _moveDirection;

            CheckBorders();

            if (!_gameSettings.godMode && _snakePathGridPositions.Contains(_snakeHeadGridPos))
            {
                EndGame(false);
                return;
            }

            _snakePathGridPositions.Insert(0,_snakeHeadGridPos);
            _foodSpawner.RemoveUnlockedGrids(_snakeHeadGridPos);

            if (_snakeHeadGridPos == _foodSpawner.foodPos)
            {
                AddSnakeSize();
                
                if (_foodSpawner.GetUnlockedGridsLength() == 0)
                {
                    EndGame(true);
                    return;
                }

                _foodSpawner.SpawnFood();
            }
            else
            {
                if (_gameSettings.godMode)
                {
                   if(_snakePathGridPositions.FindAll(delegate(Vector2Int v) { return v == _snakePathGridPositions[_snakePathGridPositions.Count - 1]; }).Count == 1)
                       _foodSpawner.AddUnlockedGrids(_snakePathGridPositions[_snakePathGridPositions.Count - 1]);
                }
                else  _foodSpawner.AddUnlockedGrids(_snakePathGridPositions[_snakePathGridPositions.Count - 1]);

                _snakePathGridPositions.RemoveAt(_snakePathGridPositions.Count - 1);
                
            }

            for (int i = 0; i < _snakePathGridPositions.Count; i++)
            {
                 _snakePartsTransforms[i].position = _snakePathGridPositions[i] + _offset;
            }
            
            if (_directionChanged)
            {
                _directionChanged = false;
                _timer = 0;
            }else _timer -= _moveSpeed;
        }
    }

    private void EndGame(bool win)
    {
        _mooving = false;
        if (win)
        {
        }
        else
        {
            {
            }
        }//todo
        gameOver.Invoke();
    }

    private void CheckBorders()
    {
        if (_snakeHeadGridPos.x > _gameSettings.gridSize.x - 1) _snakeHeadGridPos.x = 0;
        else if (_snakeHeadGridPos.x < 0) _snakeHeadGridPos.x = _gameSettings.gridSize.x - 1;
        if (_snakeHeadGridPos.y > _gameSettings.gridSize.y - 1) _snakeHeadGridPos.y = 0;
        else if (_snakeHeadGridPos.y < 0) _snakeHeadGridPos.y = _gameSettings.gridSize.y - 1;
    }

    private void AddSnakeSize()
    {
        snakeSize.Variable.RuntimeValue++;

        if (snakeSize % 5 == 0) _moveSpeed = _moveSpeed / 1.1f;
        
        var t = Instantiate(_snakePartPrefab).transform;
        
        if (!_gameSettings.singleColor)
        {
            var mesh = Instantiate(_meshFilter.mesh);
            SetMeshVertexColor(_foodSpawner.foodColor);
            mesh.colors = _colors;
            t.GetComponent<MeshFilter>().mesh = mesh;
        }
        
        t.localScale = new Vector3(0.8f,0.8f,0.8f);
        _snakePartsTransforms.Add(t);
        
        snakeGrow.Invoke();
    }

    public void SetNewMoveDirection(Vector2 direction)
    {
        if (direction.x != 0 && direction.y != 0 || _moveDirection == direction || _moveDirection == -direction) return;
        
        if (_moveDirection.x == 0 && direction.x != 0
            || _moveDirection.y == 0 && direction.y != 0) _moveDirection = new Vector2Int((int)direction.x, (int)direction.y);
        _directionChanged = true;
    }

    private void SetMeshVertexColor(Color col)
    {
        for (int i = 0; i < _colors.Length; i++) _colors[i] = col;
    }

}
