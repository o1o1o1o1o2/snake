using UnityEngine;

public enum MaskingType
{
    Pixels,
    Percents
}

[ExecuteInEditMode]
public class CameraSetup : MonoBehaviour
{
    [SerializeField] private MaskingType _maskingType;

    [SerializeField, SerializeProperty("horizontalOffset")]
    private int _horizontalOffset = 10;

    public int horizontalOffset
    {
        get => _horizontalOffset;
        private set
        {
            if (value >= 0)
            {
                _horizontalOffset = value;
                SetupCamera();
            }
        }
    }

    [SerializeField, SerializeProperty("topOffset")]
    private int _topOffset = 10;

    public int topOffset
    {
        get => _topOffset;
        private set
        {
            if (value >= 0)
            {
                _topOffset = value;
                SetupCamera();
            }
        }
    }

    private Camera _camera;
    private Transform _cameraTransform;
    private GameSettings _gameSettings;

    public void Constructor(GameSettings gameSettings)
    {
        _gameSettings = gameSettings;
    }

    public void SetupCamera()
    {
        if (_camera != null)
        {
            _cameraTransform.position = new Vector3((float) _gameSettings.gridSize.x / 2, (float) _gameSettings.gridSize.y / 2, -10);

            float workingWidthInPixels;
            Vector3 topOffset;

            if (_maskingType == MaskingType.Percents) workingWidthInPixels = _camera.pixelWidth - (float) _camera.pixelWidth / 100 * _horizontalOffset * 2;
            else workingWidthInPixels = _camera.pixelWidth - _horizontalOffset * 2;

            if (workingWidthInPixels > 0) _camera.orthographicSize = _camera.pixelWidth / (workingWidthInPixels / _gameSettings.gridSize.x);

            if (_maskingType == MaskingType.Percents) topOffset = new Vector3(0, (float) _topOffset / 100 / _camera.aspect * _camera.orthographicSize, 0);
            else topOffset = new Vector3(0, (float) _topOffset / _camera.pixelHeight / _camera.aspect * _camera.orthographicSize, 0);

            topOffset -= new Vector3(0, (_camera.orthographicSize / _camera.aspect - _gameSettings.gridSize.y) / 2, 0);
            _cameraTransform.position += topOffset;
        }
    }

    public void Awake()
    {
        _camera = Camera.main;
        if (_camera != null) _cameraTransform = _camera.transform;
        SetupCamera();
    }
}
