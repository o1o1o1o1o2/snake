using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[ExecuteInEditMode]
public class LevelMap : MonoBehaviour
{
    private GameSettings _gameSettings;
    private SpriteRenderer _mapSpriteRenderer;

    public void Constructor(GameSettings gameSettings)
    {
        _gameSettings = gameSettings;
    }

    private void Awake()
    {
        _mapSpriteRenderer = GetComponent<SpriteRenderer>();
        SetupGrids();
    }

    public void SetupGrids()
    {
        transform.position = new Vector3((float)_gameSettings.gridSize.x/2,(float)_gameSettings.gridSize.y/2);
        _mapSpriteRenderer.size = _gameSettings.gridSize;
    }
    
}
