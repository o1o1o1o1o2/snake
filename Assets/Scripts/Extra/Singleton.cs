using UnityEngine;


public class Singleton<T> : MonoBehaviour where T : Component
{
    private static T _instance;

    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<T>();;
                if (_instance == null) Debug.Log("Singleton not found " + typeof(T).ToString());
            }
            return _instance;
        }
        set => _instance = value;
    }

    private void OnDestroy()
    {
        if (_instance == this) _instance = null;
    }
}
