using System;
using UnityEngine;

[Serializable]
public class ScriptableVariable<T> : ScriptableObject, ISerializationCallbackReceiver where T : struct
{
    public T InitialValue;

    [NonSerialized] 
    public T RuntimeValue;

    public void OnAfterDeserialize()
    {
        RuntimeValue = InitialValue;
    }

    public void ReInit()
    {
        RuntimeValue = InitialValue;
    }

    public void OnBeforeSerialize()
    {
    }
}
