using System;
using UnityEngine;


[Serializable]
public class NumericReference<T>  where T : struct
{
    public bool UseConstant = true;

    public T ConstantValue;

    public ScriptableVariable<T> Variable;

    public NumericReference() { }
    public NumericReference(T value)
    {
        UseConstant = true;
        ConstantValue = value;
    }

    public T Value => UseConstant ? ConstantValue : Variable.RuntimeValue;

    public static implicit operator T(NumericReference<T> Reference)
    {
        return Reference.Value;
    }
    
    public static implicit operator NumericReference<T>(T Value)
    {
        return new NumericReference<T>(Value);
    }
    
}

[Serializable]
public class FloatReference : NumericReference<float>
{
    public FloatReference(float Value) : base(Value) { }
    public FloatReference() { }
    public static implicit operator float(FloatReference Reference)
    {
        return Reference.Value;
    }
    public static implicit operator FloatReference(float Value)
    {
        return new FloatReference(Value);
    }
}


[Serializable]
public class IntReference : NumericReference<int>
{
    public IntReference(int Value) : base(Value) { }
    public IntReference() { }
    public static implicit operator int(IntReference Reference)
    {
        return Reference.Value;
    }
    public static implicit operator IntReference(int Value)
    {
        return new IntReference(Value);
    }
}

[Serializable]
public class Vector3Reference : NumericReference<Vector3>
{
    public Vector3Reference(Vector3 Value) : base(Value) { }
    public Vector3Reference() { }
    public static implicit operator Vector3(Vector3Reference Reference)
    {
        return Reference.Value;
    }
    public static implicit operator Vector3Reference(Vector3 Value)
    {
        return new Vector3Reference(Value);
    }
}


