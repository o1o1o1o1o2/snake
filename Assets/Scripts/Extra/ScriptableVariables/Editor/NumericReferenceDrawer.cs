using System;
using UnityEditor;
using UnityEngine;

/// <summary>
/// ReferenceDrawer Class.
/// </summary>
[CustomPropertyDrawer(typeof(NumericReference<>), true)]
public class NumericReferenceDrawer<T> : PropertyDrawer where T : struct
{
    /// <summary>
    /// Options to display in the popup to select constant or variable.
    /// </summary>
    private readonly string[] _PopupOption = {"Use Constant", "Use Variable"};

    /// <summary> Cached style to use to draw the popup button. </summary>
    private GUIStyle _PopupStyle;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (_PopupStyle == null)
        {
            _PopupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"));
            _PopupStyle.imagePosition = ImagePosition.ImageOnly;
        }

        label = EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, label);
        var labelPosition = position;

        EditorGUI.BeginChangeCheck();

        // Get properties
        SerializedProperty useConstant = property.FindPropertyRelative("UseConstant");
        SerializedProperty constantValue = property.FindPropertyRelative("ConstantValue");
        SerializedProperty variable = property.FindPropertyRelative("Variable");

        // Calculate rect for configuration button
        position.xMin = labelPosition.xMin/2;
        position.xMax = labelPosition.xMin;
        Rect buttonRect = new Rect(position);
        buttonRect.yMin += _PopupStyle.margin.top;
        buttonRect.width = _PopupStyle.fixedWidth + _PopupStyle.margin.right;
        position.xMin = buttonRect.xMax;
        if (!useConstant.boolValue) position.xMax = labelPosition.xMin;
        else position.xMax = labelPosition.xMax;

        // Store old indent level and set it to 0, the PrefixLabel takes care of it
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;
        int result = EditorGUI.Popup(buttonRect, useConstant.boolValue ? 0 : 1, _PopupOption, _PopupStyle);
        useConstant.boolValue = result == 0;
        EditorGUI.PropertyField(position, useConstant.boolValue ? constantValue : variable, GUIContent.none);

        if (!useConstant.boolValue)
        {
            ScriptableVariable<T> val = variable.objectReferenceValue as ScriptableVariable<T>;
          
            if (val !=  null)
            {
                position.xMin = position.xMax + 10;
                position.xMax = labelPosition.xMax;
                DrawValue(variable, position, val);
            }
        }
        
        if (EditorGUI.EndChangeCheck())
            property.serializedObject.ApplyModifiedProperties();

        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }
    
    private void DrawValue(SerializedProperty property, Rect position, ScriptableVariable<T> val)
    {
        switch (val.GetType().ToString())
        {
            case "FloatVariable":
            {
                float f = (float) Convert.ChangeType(val.InitialValue, typeof(float));
                f = SerializedPropertyExt.MyFloatFieldInternal(position, new Rect(position.x-10, position.yMin, 10, position.height), f, EditorStyles.numberField);
                ScriptableVariable<float> v = val as ScriptableVariable<float>;
                v.InitialValue = f;
                break;
            }
            case "IntVariable":
            {
                int i = (int) Convert.ChangeType(val.InitialValue, typeof(int));
                i = SerializedPropertyExt.MyIntFieldInternal(position, new Rect(position.x-10, position.yMin, 10, position.height), i, EditorStyles.numberField);
                ScriptableVariable<int> v = val as ScriptableVariable<int>;
                v.InitialValue = i;
                break;
            }
            case "Vector3Variable":
            {
                Vector3 v3 = (Vector3) Convert.ChangeType(val.InitialValue, typeof(Vector3));
                v3 = EditorGUI.Vector3Field(position, GUIContent.none, v3);
                ScriptableVariable<Vector3> v = val as ScriptableVariable<Vector3>;
                v.InitialValue = v3;
                break;
            }
        }
    }
}

[CustomPropertyDrawer(typeof(FloatReference), true)]
public class FloatVariableReferenceDrawer : NumericReferenceDrawer<float>
{
}

[CustomPropertyDrawer(typeof(IntReference), true)]
public class IntVariableReferenceDrawer : NumericReferenceDrawer<int>
{
}

[CustomPropertyDrawer(typeof(Vector3Reference), true)]
public class Vector3VariableReferenceDrawer : NumericReferenceDrawer<Vector3>
{
}
