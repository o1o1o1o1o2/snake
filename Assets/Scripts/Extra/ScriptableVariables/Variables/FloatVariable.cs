using UnityEngine;

[CreateAssetMenu(fileName = "FloatVariable", menuName = "SO/FloatVariable", order = 1)]
public class FloatVariable : ScriptableVariable<float>
{ 
}