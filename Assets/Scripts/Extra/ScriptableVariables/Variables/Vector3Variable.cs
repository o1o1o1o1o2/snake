using UnityEngine;

[CreateAssetMenu(fileName = "Vector3Variable", menuName = "SO/Vector3Variable", order = 1)]
public class Vector3Variable : ScriptableVariable<Vector3>
{
}