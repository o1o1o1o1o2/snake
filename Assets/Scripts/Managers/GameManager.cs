using UnityEngine;

[DefaultExecutionOrder(-10)]
[ExecuteInEditMode]
public class GameManager : MonoBehaviour
{
    [SerializeField] private GameSettings gameSettings;
    [SerializeField] private SnakeMovement snakeMovement;
    [SerializeField] private SnakeInputController snakeInputController;
    [SerializeField] private LevelMap map;
    [SerializeField] private CameraSetup cameraSetup;

    private FoodSpawner _foodSpawner;

    [SerializeField] private GameObject snakePrefab;
    [SerializeField] private GameObject foodPrefab;

    private void Awake()
    {
        OnlyRuntimeConstruction();
        
        snakeMovement.Constructor(gameSettings, _foodSpawner, snakePrefab);
        snakeInputController.Constructor(snakeMovement, gameSettings);
        map.Constructor(gameSettings);
        cameraSetup.Constructor(gameSettings);
    }

    private void OnlyRuntimeConstruction()
    {
        if (Application.isPlaying)
        {
            _foodSpawner = new FoodSpawner(foodPrefab, gameSettings);
        }
    }

}
