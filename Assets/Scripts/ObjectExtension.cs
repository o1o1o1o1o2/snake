using System.Linq;
using UnityEngine;

public static class ObjectExtension 
{
    public static T FindObjectWithNotActive<T>() where T: Object
    {
        return Resources.FindObjectsOfTypeAll(typeof(T)).FirstOrDefault(go => go.hideFlags == HideFlags.None) as T;
    }
}
