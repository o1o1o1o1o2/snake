using UnityEngine;
using UnityEngine.UIElements;

[RequireComponent(typeof(UIDocument))]
public class ScreenSwitcher : Singleton<ScreenSwitcher>
{
    private UIDocument _uiDocument;
    private VisualElement _rootVisualElement;

    [SerializeField]
    private UIScreen currentScreen;
    
    private void Awake()
    {
        _uiDocument = GetComponent<UIDocument>();
        
        if (_uiDocument.visualTreeAsset != currentScreen.screenTreeAsset) _uiDocument.visualTreeAsset = currentScreen.screenTreeAsset;
        
        currentScreen.gameObject.SetActive(true);
    }

    public void SetNewScreen(UIScreen screen)
    {
        currentScreen.gameObject.SetActive(false);
        
        currentScreen = screen;

        _uiDocument.visualTreeAsset = currentScreen.screenTreeAsset;
        
        _rootVisualElement = _uiDocument.rootVisualElement;
        PointerOverUIElement.RegisterAllUIElementsInRoot(_rootVisualElement);
        
        currentScreen.gameObject.SetActive(true);
    }


}
