using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

[DefaultExecutionOrder(-20)]
public class UIScreen: MonoBehaviour
{
    [SerializeField]
    public VisualTreeAsset screenTreeAsset;
    
    protected UIDocument uiDocument;
    
    protected VisualElement rootVisualElement;

    protected virtual void Awake()
    {
        uiDocument = GetComponentInParent<UIDocument>();
        if (screenTreeAsset == null)
        {
            var name = GetType().Name;
            string[] ss = AssetDatabase.FindAssets($"{name} t:VisualTreeAsset", new[] {"Assets/Resources/Ui"});
            screenTreeAsset = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(AssetDatabase.GUIDToAssetPath(ss[0]));
        }
        if (uiDocument.visualTreeAsset != screenTreeAsset) gameObject.SetActive(false);
    }
    
    
    private void OnEnable()
    {
        if (uiDocument.visualTreeAsset != screenTreeAsset) return;
        Enable();
    }

    protected virtual void Enable()
    {
    }
    
    private void OnDisable()
    {
        if (uiDocument.visualTreeAsset != screenTreeAsset) return;
        Disable();
    }


    protected virtual void Disable()
    {
    }
}
