using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

public class GameProcessScreen : UIScreen
{
    [SerializeField] private UnityEvent _stopGame;
    [SerializeField]
    private IntReference _scores;
    [SerializeField]
    private IntReference _totalScores;
    private Label _scoresLabel;
    private Label _totalScoresLabel;
    private Button _buttonUp;
    private Button _buttonLeft;
    private Button _buttonRight;
    private Button _buttonDown;
    private Button _buttonStopGame;

    protected override void Enable()
    {
        rootVisualElement = uiDocument.rootVisualElement;
        _scoresLabel = rootVisualElement.Q<Label>("scores");
        _totalScoresLabel = rootVisualElement.Q<Label>("totalScores");
        
         _scoresLabel.text = _scores.Variable.InitialValue.ToString();
        _totalScoresLabel.text = _totalScores.Variable.InitialValue.ToString();
        
        _buttonUp = rootVisualElement.Q<Button>("buttonUp");
        _buttonLeft = rootVisualElement.Q<Button>("buttonLeft");
        _buttonRight = rootVisualElement.Q<Button>("buttonRight");
        _buttonDown = rootVisualElement.Q<Button>("buttonDown");
        _buttonStopGame = rootVisualElement.Q<Button>("ButtonStopGame");
        
        _buttonUp.RegisterCallback<ClickEvent>(ev => SnakeInputController.Instance.ButtonUpPressed());
        _buttonLeft.RegisterCallback<ClickEvent>(ev => SnakeInputController.Instance.ButtonLeftPressed());
        _buttonRight.RegisterCallback<ClickEvent>(ev => SnakeInputController.Instance.ButtonRightPressed());
        _buttonDown.RegisterCallback<ClickEvent>(ev => SnakeInputController.Instance.ButtonDownPressed());
        _buttonStopGame.RegisterCallback<ClickEvent>(ev => StopGame());
    }

    protected override void Disable()
    {
        _buttonUp.UnregisterCallback<ClickEvent>(ev => SnakeInputController.Instance.ButtonUpPressed());
        _buttonLeft.UnregisterCallback<ClickEvent>(ev => SnakeInputController.Instance.ButtonLeftPressed());
        _buttonRight.UnregisterCallback<ClickEvent>(ev => SnakeInputController.Instance.ButtonRightPressed());
        _buttonDown.UnregisterCallback<ClickEvent>(ev => SnakeInputController.Instance.ButtonDownPressed());
        
        _buttonStopGame.UnregisterCallback<ClickEvent>(ev => StopGame());
        
        _totalScores.Variable.InitialValue = _totalScores.Variable.RuntimeValue;
    }

    public void UpdateScores()
    {
        _scoresLabel.text = _scores.Variable.RuntimeValue.ToString();
        _totalScores.Variable.RuntimeValue++;
        _totalScoresLabel.text = _totalScores.Variable.RuntimeValue.ToString();
    }

    private void StopGame()
    {
        _stopGame.Invoke();
    }
}
