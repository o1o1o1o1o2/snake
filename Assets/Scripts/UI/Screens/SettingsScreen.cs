using UnityEngine;
using UnityEngine.UIElements;

public class SettingsScreen : UIScreen
{
    [SerializeField] private GameSettings _settings;
    private Button _backButton;
    private Toggle _godModeToggle;

    protected override void Enable()
    {
        rootVisualElement = uiDocument.rootVisualElement;
        _backButton = rootVisualElement.Q<Button>("BackButton");
        _godModeToggle = rootVisualElement.Q<Toggle>("GodModeToggle");
        _godModeToggle.value = _settings.godMode;
        _backButton.RegisterCallback<ClickEvent>(ev => OpenStartScreen());
        _godModeToggle.RegisterCallback<ClickEvent>(ev => ToggleGodMode());
    }

    
    protected override void Disable()
    {
        _backButton.UnregisterCallback<ClickEvent>(ev => OpenStartScreen());
        _godModeToggle.UnregisterCallback<ClickEvent>(ev => ToggleGodMode());
    }

    private void ToggleGodMode()
    {
        _settings.godMode = _godModeToggle.value;
    }

    private void OpenStartScreen()
    {
        ScreenSwitcher.Instance.SetNewScreen(ObjectExtension.FindObjectWithNotActive<StartScreen>());
    }
}
