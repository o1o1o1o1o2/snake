using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

public class StartScreen : UIScreen
{
    [SerializeField] private UnityEvent gameStarted;
    private Button _startButton;
    private Button _settingsButton;

    protected override void Enable()
    {
        rootVisualElement = uiDocument.rootVisualElement;
        _startButton = rootVisualElement.Q<Button>("StartButton");
        _settingsButton = rootVisualElement.Q<Button>("SettingsButton");
        _startButton.RegisterCallback<ClickEvent>(ev => StartGame());
        _settingsButton.RegisterCallback<ClickEvent>(ev => OpenSettings());
    }
    
    protected override void Disable()
    {
        _startButton.UnregisterCallback<ClickEvent>(ev => StartGame());
        _settingsButton.UnregisterCallback<ClickEvent>(ev => OpenSettings());
    }

    private void OpenSettings()
    {
        ScreenSwitcher.Instance.SetNewScreen(ObjectExtension.FindObjectWithNotActive<SettingsScreen>());
    }

    private void StartGame()
    {
        ScreenSwitcher.Instance.SetNewScreen(ObjectExtension.FindObjectWithNotActive<GameProcessScreen>());
        gameStarted.Invoke();
    }
    
  
}
